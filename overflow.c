#include <stdio.h>
#include <stdlib.h>

typedef struct ProgramData{
	char initials[3];
	char command[20];
} ProgramData;

int main(void) {
	
	ProgramData someData;
	someData.command[0]='l';
	someData.command[1]='s';
	someData.command[2]='\0'; //termination character
	
	printf("Stored Command: %s \n", someData.command);
	system(someData.command);

	printf("Please enter your initials: ");
	// gets est une méthode dangeureuse qui va être à l'origine du débordement tampon
	gets(someData.initials);

	printf("Your initials: %s, Stored Command: %s \n", someData.initials, someData.command);
	system(someData.command);

	return 0;
}
