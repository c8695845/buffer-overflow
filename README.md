Permet de voir l'exploitation simple d'une faille débordement tampon.
```
gcc overflow.c -o overflow
./overflow
```
En exécutant le programme, tout le texte indiqué au delà de 3 caractères débordera dans someData.command dont l'espace mémoire est accolé à celui de someData.initials. Nous permettant d'exécuter une commande arbitraire sur le système.
